import 'whatwg-fetch'
import fetchMock from 'fetch-mock'
import Vue from 'vue'
import App from 'src/crawing/mobile/App.vue'
import 'babel-polyfill'
import 'es6-promise'
import BaseComponent from 'src/components'
import webApi from '../../../../src/libs/webapi'

fetchMock.get('*', {hello: 'world'})

Vue.use(BaseComponent)

describe('crawing/mobile/App.vue', () => {

  window.App = new Vue({
    template: '<div><app></app></div>',
    components: { App }
  }).$mount()

  it('初始状态只显示手机号码', () => {
    const vm = window.App.$children[0]
    let state = vm.$data.state
    expect(state.mobile).to.equal(true)
    expect(state.serverPass).to.equal(false)
    expect(state.webPass).to.equal(false)
    expect(state.catchCode).to.equal(false)
    expect(state.msgCode).to.equal(false)
  })

  it('前端表单验证', (done)=> {
    const vm = window.App.$children[0]
    let formData = vm.$data.formData
    expect(formData.mobile).to.equal('')
    expect(vm.toasts.length).to.equal(0)
    //点击提交
    vm.handleSubmit()

    vm.$nextTick(() => {
      expect(vm.toasts.length).to.equal(1)
      done()
    })
  })
  it('app向h5传递参数得到用户手机号码，并自动填写', (done) => {
    const vm = window.App.$children[0]
    let formData = vm.$data.formData
    window.nativeBriage.authPNumber = '13522287687'
    vm.$nextTick(() => {
      expect(formData.mobile).to.equal('13522287687')
      done()
    })
  })

  it('checkPhoneNumber', (done) => {
    const vm = window.App.$children[0]

    // fetchMock.mock(webApiConfig.mobile.path,
    //   {"code":"1","msg":"","status":"0", "body": {errorCode: '00000',captchaNeeded: false}})

    webApi.mobile.checkPhoneNumber = () => Promise.resolve(
      {code:'1',msg:'',status:'0', body: {errorCode: '00000',captchaNeeded: false}}
      )

    vm.handleCheckPhone()
      .then(()=> {
        vm.$nextTick(()=> {
          expect(vm.currentStep).to.equal(2)
          done()
        })
      })
    // vm.getCatchCode()
    // vm.getRandomcode()
    // vm.handleLogin()
    // vm.handleBeforeBill()
    // vm.billForm()
    // vm.handleBill()

  })




})
