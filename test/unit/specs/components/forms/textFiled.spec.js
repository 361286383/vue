import Vue from 'vue'
import textField from 'src/components/forms/textField'

describe('textField.vue', () => {
  it('should render correct contents', () => {
    const vm = new Vue({
      template: '<div><text-field label="demo"></text-field></div>',
      components: { textField }
    }).$mount()

    let el = vm.$el.querySelector('.item-form')
    expect(el.textContent).to.contain('demo')
    expect(el.innerHTML).to.not.contain('item-form-icon')
  })

  it('should has icon', () => {
    const vm = new Vue({
      template: '<div><text-field label="phone" icon="phone"></text-field></div>',
      components: { textField }
    }).$mount()

    let el = vm.$el.querySelector('.item-form')
    expect(el.textContent).to.contain('phone')
    expect(el.innerHTML).to.contain('item-form-icon')
  })

  it('should has textarea', () => {
    const vm = new Vue({
      template: '<div><text-field  type="textarea"></text-field></div>',
      components: { textField }
    }).$mount()

    let el = vm.$el.querySelector('textarea')
    expect(el).to.be.ok
  })
})
