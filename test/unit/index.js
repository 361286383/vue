// Polyfill fn.bind() for PhantomJS
/* eslint-disable no-extend-native */
Function.prototype.bind = require('function-bind')

// require all test files (files that ends with .spec.js)
/**
 * You can create your own context with the require.context function.
 * It allow to pass a directory, regular expression and a flag if subdirectories should be used too.
 * require.context(directory, useSubdirectories = false, regExp = /^\.\//)
 */
var testsContext = require.context('./specs', true, /\.spec$/)
testsContext.keys().forEach(testsContext)

// 排除 src/home  src/components/carbon src/example
var srcContext = require.context('../../src', true, /^\.\/(?!((components\/carbon.+)|(example\/.+)|(home\/.+)))/)
srcContext.keys().forEach(srcContext)

// 输出所有符合合条件的
// console.log(srcContext.keys())
