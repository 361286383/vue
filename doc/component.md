# 组件使用说明

## forms/textField 组件

textField 封装了大部分表单输入框, 支持的输入框类型如下

组件属性:

| 属性名称  | 说明
|---|
| lable  | `string` 标签名
| icon  | `string` 右侧图标,比如 icon="phone" , 默认不显示图标
| type  | `string` 类型  可选 date,email,number,password,search,tel,text,time,url
| placeholder  | `string` 标签名
| value  | `string` 可以指定输入框的初始值
| switch  | `string` 星号与明文密码开关选项


## forms/formGroup 组件

textField 封装了大部分表单输入框, 支持的输入框类型如下

组件属性:

| 属性名称  | 说明
|---|
| lable  | `string` 标签名
| icon  | `string` 右侧图标,比如 icon="phone" , 默认不显示图标
| type  | `string` 类型  可选 date,email,number,password,search,tel,text,time,url
| placeholder  | `string` 标签名
| value  | `string` 可以指定输入框的初始值
| switch  | `string` 星号与明文密码开关选项


## button/button 组件

textField 封装了大部分表单输入框, 支持的输入框类型如下

组件属性:

| 属性名称  | 说明
|---|
| lable  | `string` 标签名
| icon  | `string` 右侧图标,比如 icon="phone" , 默认不显示图标
| type  | `string` 类型  可选 date,email,number,password,search,tel,text,time,url
| placeholder  | `string` 标签名
| value  | `string` 可以指定输入框的初始值
| switch  | `string` 星号与明文密码开关选项








