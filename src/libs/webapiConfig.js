// const BaseUrl= 'http://123.59.79.91:8080/data'
// const BaseUrl= 'http://app-h5.iqianzhan.com/api/data'
const BaseUrl = process.env.NODE_ENV === 'development' ? '/api/data' : 'http://172.16.37.88:3000/api/data'
const ziMaConfig = {
  development: {
    url: 'http://zmxy-node.jiea.iqianjin.com/zmxy/web/index?userId='
  },
  production: {
    url: 'http://app-zmxy.iqianzhan.com/zmxy/web/index?userId='
  },
  testing: {
    url: 'http://zmxy-node.jiea.iqianjin.com/zmxy/web/index?userId='
  }
}
const config = {
  /** 手机运营商 */
  mobile: {
    checkPhoneNumber: {
      path: `${BaseUrl}/mobile/checkPhoneNumber`,
      method: 'post',
      description: '校验手机号'
    },
    captcha: {
      path: `${BaseUrl}/mobile/captcha`,
      method: 'post',
      description: '获取登录时的图片验证码'
    },
    secondCaptcha: {
      path: `${BaseUrl}/mobile/secondCaptcha`,
      method: 'post',
      description: '获取二次图片验证码，即发送短信所需要的图片验证码（添加两个参数 applyNo和token）'
    },

    sendLoginRandomcode: {
      path: `${BaseUrl}/mobile/sendLoginRandomcode`,
      method: 'post',
      description: '获取登录短信密码'
    },
    sendRandomcode: {
      path: `${BaseUrl}/mobile/sendRandomcode`,
      method: 'post',
      param: {
        captchaCode: String
      },
      description: '发送随机短信'
    },

    login: {
      path: `${BaseUrl}/mobile/login`,
      method: 'post',
      description: '登录（添加两个参数 applyNo和token）'
    },
    bills: {
      path: `${BaseUrl}/mobile/bills`,
      param: {
        randomCode: String,
        captchaCode: String
      },
      description: '抓取用户数据请求，这一步必须要执行，否则这边不会去执行对用户数据的抓取（添加两个参数 applyNo和token）'
    }
  },
  /** 信用卡邮箱 */
  mail: {
    creditCardLogin: {
      path: `${BaseUrl}/creditCard/creditCardLogin`,
      method: 'post',
      param: {
        email: String,
        passwd: String
      },
      description: '信用卡邮箱登录'
    }
  },

  /** 银行流水 */
  bank: {
    isSupportBank: {
      path: `${BaseUrl}/bankDebit/isSupportBank`,
      method: 'post',
      param: {
        bankType: String,   //【必填】银行类型（借记卡:1,信用卡:2）
        bankName: String,   //
      },
      description: '判断是否支持该银行'
    },
    getValiCode: {
      path: `${BaseUrl}/bankDebit/bankDebitGetValiCode`,
      method: 'post',
      param: {
        bankType: String,
        currentTime: String
      },
      description: '获取银行验证码'
    },
    loginBank: {
      path: `${BaseUrl}/bankDebit/loginBank`,
      method: 'post',
      param: {
        //applyNo: String,
        bankType: String,
        captchaCode: String,
        userName: String, // 对应 3种登陆方式 身份证/登陆名/卡号
        batchNo: String,
        accountNo: String, // 已弃用
        loginType: String,
        passWord: String
      },
      description: '获取账户信息及最近6个月的流水'
    },
    bankDebitMessage: {
      path: `${BaseUrl}/bankDebit/bankDebitMessage`,
      method: 'post',
      param: {
        bankType: String,
      },
      description: '浦发和光大银行,获取银行手机动态验证码'
    },
    login2Bank: {
      path: `${BaseUrl}/bankDebit/login2Bank`,
      method: 'post',
      param: {
        bankType: String,
        captchaCode: String,
        userName: String,
        batchNo: String, // 可为空
        messageCode: String,
        accountNo: String, // 可为空
      },
      description: `浦发和光大银行,输入手机动态验证码，获取账户信息及最近6个月的流水`
    },
    // 已弃用
    getBankDebitRS: {
      path: `${BaseUrl}/bankDebit/getBankDebitRS`,
      method: 'post',
      param: {
        batchNo: String,
        version: String
      },
      description: `获取银行流水结果是否获取成功
      返回结果:1:未上传,2:待质检,3:质检通过,4:质检未通过,5:获取中,6:获取成功,7:获取失败,8:线下提交;
      `
    },

  },

  /** 征信报告 */
  report: {
    /*老用户登录过程接口*/
    loginOldCaptcha: {
      path: `${BaseUrl}/creditReport/captcha`,
      method: 'post',
      param: {
        productType: '102'//产品类型
      },
      description: '获取老用户登录时的图片验证码'
    },
    loginOld: {
      path: `${BaseUrl}/creditReport/login`,
      method: 'post',
      param: {
        productType: '102', //产品类型
        loginName: String, //必填登录名
        passWord: String, //必填 密码
        captchaCode: String //必填 图片验证码
      },
      description: '老用户登录功能'
    },
    oldGetPhoneCode: {
      path: `${BaseUrl}/creditReport/sendPhoneMsg`,
      method: 'post',
      param: {

      },
      description: '老用户登录成功重新获取短信验证码'
    },
    oldapplyCode: {
      path: `${BaseUrl}/creditReport/submitKBA`,
      method: 'post',
      param: {
        questions: String //
      },
      description: '老用户登录成功,回答问题申请查询码'
    },
    oldSubapplyCode: {
      path: `${BaseUrl}/creditReport/submitQS`,
      method: 'post',
      param: {
        htmlToken: String,
        verifyCode: String
      },
      description: '老用户登录成功,输入手机动态码申请查询码'
    },
    oldDownReport: {
      path: `${BaseUrl}/creditReport/downloadCreditR`,
      method: 'post',
      param: {
        tradeCode: String,
        htmlToken: String,
        loginName: String
      },
      description: '老用户登录成功,输入查询码下载征信报告'
    },
    /*新用户注册过程中所用接口*/
    newSubIdNo: {
      path: `${BaseUrl}/creditReport/register`,
      method: 'post',
      param: {
        productType: '102',
        name: String,
        idType: String,
        idNo: String,
        captchaCode: String
      },
      description: '新用户注册，第一步：填写证件信息'
    },
    newVerifyName: {
      path: `${BaseUrl}/creditReport/checkRegLoginnameHasUsed`,
      method: 'post',
      param: {
        productType: '102',
        loginName: String
      },
      description: '新用户注册，第二步：验证登录名是否存在'
    },
    newRegisterGetPhoneCode: {
      path: `${BaseUrl}/creditReport/getMobileVerifyCode`,
      method: 'post',
      param: {
        productType: '102',
        mobileTel: String
      },
      description: '新用户注册，第三步：获取手机验证码'
    },
    newSubsupplement: {
      path: `${BaseUrl}/creditReport/saveUser`,
      method: 'post',
      param: {
        productType: '102',
        loginName: String,
        passWord: String,
        confirmPassWord: String,
        mobileTel: String,
        email: String,
        verifyCode: String,
        tcId: String,
        htmlToken: String
      },
      description: '新用户注册，第四步：提交补充信息，完成注册'
    },
    /*找回登录名*/
    findLoginName: {
      path: `${BaseUrl}/creditReport/findLoginName`,
      method: 'post',
      param: {
        productType: '102',
        name: String,
        idType: String,
        idNo: String,
        captchaCode: String
      },
      description: '找回登录名，填写用户信息'
    },
    /*找回密码*/
    fpSubLoginName: {
      path: `${BaseUrl}/creditReport/checkLoginName`,
      method: 'post',
      param: {
        productType: '102',
        loginName: String,
        name: String,
        idType: String,
        idNo: String,
        captchaCode: String
      },
      description: '找回密码，第一步：填写登录名'
    },
    fpGetPhoneCode: {
      path: `${BaseUrl}/creditReport/resetPasswordMobileVerifyCode`,
      method: 'post',
      param: {

      },
      description: '找回密码，第二步：获取手机验证码'
    },
    fpSetPassword: {
      path: `${BaseUrl}/creditReport/resetPassword`,
      method: 'post',
      param: {
        productType: '102',
        passWord: String,
        confirmPassWord: String,
        verifyCode: String,
        idNo: String,
        captchaCode: String
      },
      description: '找回密码，第三步：填写新密码'
    },
    fpSubQuestion: {
      path: `${BaseUrl}/creditReport/saveKbaApply`,
      method: 'post',
      param: {
        questions: String
      },
      description: '找回密码，第四步：提交问题'
    }
  },
  /** 芝麻信用 */
  ziMa: {
    path: ziMaConfig[process.env.NODE_ENV].url
  },
}
export default {
  BaseUrl,
  ...config
}
