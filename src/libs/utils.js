/**
 * 序列化参数
 * 将 js object 如 {pid:867830024077556, token:abcd} 序列化为 pid=867830024077556&token=abcd
 * @param obj
 * @returns {string}
 */
export function serialize(obj) {
  if (!isObject(obj)) {
    throw new Error('参数必须为 {} 对象')
  }
  let pairs = []
  Object.keys(obj).map(
    (key) => {
      try{
        if(isFunction(obj[key])) obj[key] = ''
        let _value = isObject(obj[key]) || isArray(obj[key]) ? JSON.stringify(obj[key]) : obj[key].toString()
        pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(_value))
      }
      catch(e){

      }
    }
  )
  return pairs.join('&')
}
/**
 * {} Object类型判断
 */
export function isObject(v) {
  return toString.apply(v) === '[object Object]'
}
/**
 * Function类型判断
 */
export function isFunction(v) {
  return toString.apply(v) === '[object Function]'
}
/**
 * Array类型判断
 */
export function isArray(v) {
  return toString.apply(v) === '[object Array]'
}
/**
 * Json类型判断
 */
export function isJson(v) {
  return isObject(v) || isArray(v)
}

/**
 * 手机号码验证
 */

export function isMobile(it) {
  return /^0?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test(it)
}

/**
 * 身份证号码验证
 */
export function isIdientify(it) {
  return /^((1[1-5])|(2[1-3])|(3[1-7])|(4[1-6])|(5[0-4])|(6[1-5])|71|(8[12])|91)\d{4}((19\d{2}(0[13-9]|1[012])(0[1-9]|[12]\d|30))|(19\d{2}(0[13578]|1[02])31)|(19\d{2}02(0[1-9]|1\d|2[0-8]))|(19([13579][26]|[2468][048]|0[48])0229))\d{3}(\d|X|x)?$/.test(it)
}
//判断用户名必须是6-16位字母和数字组合
export function isLoginName(p) {
  return (/^(?![0-9]+$)(?![a-zA-Z]+$)[\\@A-Za-z0-9\\!\\#\\$\\%\\^\\&\\*\\.\\~]{6,16}$/).test(p);
}
