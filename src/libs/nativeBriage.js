window.nativeBriage = {
  cellPhoneType: '',
  token: '',
  pid: '',
  version: '',
  us: '',
  type: '',
  NauthIdNo: '',
  A_IdN: '',
  A_BName: '',
  authIdNo: '',
  authName: '',
  authPNumber: '',
  invitationCode: '',
  codeType: '',
  userId: '',
  isGuideFlowDone: '',
  creditCardNumber: '',
  dimCreditCardNumber: ''
}

module.exports = window.newsToken = function (data) {
  let opt = data || {}
  window.nativeBriage.cellPhoneType = opt.cellPhoneType || ''
  window.nativeBriage.token = opt.token || ''
  window.nativeBriage.pid = opt.pid || ''   // 设备id
  window.nativeBriage.version = opt.version || '' //版本
  window.nativeBriage.us = opt.us || '' //
  window.nativeBriage.type = opt.type || '' // 1:android 2: ios
  window.nativeBriage.NauthIdNo = opt.authIdNo || '' //身份证
  window.nativeBriage.A_IdN = opt.A_IdN || ''  //
  window.nativeBriage.A_BName = opt.A_BName || '' //
  window.nativeBriage.authIdNo = opt.idNo || '' //
  window.nativeBriage.authName = opt.realName || '' //真实姓名
  window.nativeBriage.authPNumber = opt.phoneNumber || '' // 手机号码
  window.nativeBriage.invitationCode = opt.invitationCode || '' //
  window.nativeBriage.codeType = opt.codeType || '' //
  window.nativeBriage.userId = opt.userId || '' //用户id
  window.nativeBriage.isGuideFlowDone = opt.isGuideFlowDone || '' //
  window.nativeBriage.creditCardNumber = opt.creditCardNumber || '' //
  window.nativeBriage.dimCreditCardNumber = opt.dimCreditCardNumber || '' //补全信用卡需要补全
}
