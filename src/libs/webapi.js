require('src/libs/nativeBriage')
import config from './webapiConfig'
import * as utils from './utils'
const merge = require('lodash/merge')


class WebApi {
  constructor() {
    // 网络异常回调
    this.NetErrorCallBack = null
    this.beforEach = null
    this.afterEach = null

    this.mobile = {}
    this.bank = {}
    this.mail = {}
    this.zima = {}
    this.report = {}

    // 代理nativeBriage
    this.nativeBriage = window.nativeBriage

    this._initMobile()
    this._initReport()
    this._initBank()
    this._initMail()
    this._initZima()

    this._getPidToken()
  }

  // ======================================
  // 私有方法
  // ======================================
  /**
   * 异步post方法
   * @param url
   * @param _param
   * @returns {promise}
   * @private
   */
  _postAsync(url, _param = {}) {
    let pid = this.pid
    let token = this.token
    _param = merge(_param, {pid, token})

    this.beforEach && this.beforEach()

    /*eslint-disable*/
    return fetch(url, {
      method: 'post',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
        // 'Content-Type': 'application/json'
      },
      body: utils.serialize(_param)
      // body: JSON.stringify(_param)
    })
      .then(response => {
        this.afterEach && this.afterEach()
        return response.json()
      })
      .catch(err => {
        if (this.NetErrorCallBack) {
          this.NetErrorCallBack()
        }
      })
    /*eslint-enable*/
  }


  /**
   * 获取 pid 和 tocken
   * @private
   */
  _getPidToken() {
    // test nativeBriage
    console.log(window.nativeBriage)
    this.pid = '99000577768988'
    this.token = '6FE700B1DD082C83BCB55BA1FB12B53AC57BFB2B3762837EF89C5F20802482BF'
  }

  // 公共 mobile api
  _initMobile() {
    this.mobile.checkPhoneNumber = (param) => this._postAsync(config.mobile.checkPhoneNumber.path, param)
    this.mobile.getCaptcha = () => this._postAsync(config.mobile.captcha.path)
    this.mobile.secondCaptcha = () => this._postAsync(config.mobile.secondCaptcha.path)
    this.mobile.sendLoginRandomcode = (param) => this._postAsync(config.mobile.sendLoginRandomcode.path)
    this.mobile.sendRandomcode = (param) => this._postAsync(config.mobile.sendRandomcode.path, param)
    this.mobile.login = (param) => this._postAsync(config.mobile.login.path, param)
    this.mobile.bills = (param) => this._postAsync(config.mobile.bills.path, param)
  }

  // 公共 bank api
  _initBank() {
    this.bank.isSupportBank = (param) => this._postAsync(config.bank.isSupportBank.path, param)
    this.bank.getValiCode = (param) => this._postAsync(config.bank.getValiCode.path, param)
    this.bank.loginBank = (param) => this._postAsync(config.bank.loginBank.path, param)
    this.bank.bankDebitMessage = (param) => this._postAsync(config.bank.bankDebitMessage.path, param)
    this.bank.login2Bank = (param) => this._postAsync(config.bank.login2Bank.path, param)
    this.bank.getBankDebitRS = (param) => this._postAsync(config.bank.getBankDebitRS.path, param)
  }

  // 公共 report api
  _initReport() {
    this.report.loginOldCaptcha = (param) => this._postAsync(config.report.loginOldCaptcha.path, param)
    this.report.loginOld = (param) => this._postAsync(config.report.loginOld.path, param)
    this.report.oldGetPhoneCode = (param) => this._postAsync(config.report.oldGetPhoneCode.path, param)
    this.report.oldapplyCode = (param) => this._postAsync(config.report.oldapplyCode.path, param)
    this.report.oldSubapplyCode = (param) => this._postAsync(config.report.oldSubapplyCode.path, param)
    this.report.oldDownReport = (param) => this._postAsync(config.report.oldDownReport.path, param)
    this.report.newSubIdNo = (param) => this._postAsync(config.report.newSubIdNo.path, param)
    this.report.newVerifyName = (param) => this._postAsync(config.report.newVerifyName.path, param)
    this.report.newRegisterGetPhoneCode = (param) => this._postAsync(config.report.newRegisterGetPhoneCode.path, param)
    this.report.newSubsupplement = (param) => this._postAsync(config.report.newSubsupplement.path, param)
    this.report.findLoginName = (param) => this._postAsync(config.report.findLoginName.path, param)
    this.report.fpSubLoginName = (param) => this._postAsync(config.report.fpSubLoginName.path, param)
    this.report.fpGetPhoneCode = (param) => this._postAsync(config.report.fpGetPhoneCode.path, param)
    this.report.fpSetPassword = (param) => this._postAsync(config.report.fpSetPassword.path, param)
    this.report.fpSubQuestion = (param) => this._postAsync(config.report.fpSubQuestion.path, param)
  }

  // 公共 mail api
  _initMail() {
    this.mail.creditCardLogin = (param) => this._postAsync(config.mail.creditCardLogin.path, param)
  }

  // 公共 zima api
  _initZima() {
    this.zima.ziMaLogin = (param) => window.location=config.ziMa.path + param.userId
  }


}

export default new WebApi()
