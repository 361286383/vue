import Vue from 'vue'
import App from './App'
import BaseComponent from 'src/components'

Vue.use(BaseComponent)
/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App }
})
