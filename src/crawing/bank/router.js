// 导入视图组件
import bankForm from './views/bankForm.vue'
import bankList from './views/bankList.vue'

export default function (router) {
  router.map({
    '/': {
      name: 'home',
      component: bankList
    },
    '/bank/:bankId': {
      name: 'bankForm',
      component: bankForm
    }
  })
}
