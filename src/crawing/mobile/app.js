/**
 * Created by 高乐天 on 16/10/31.
 */
import * as utils from '../../libs/utils'
import webApi from 'src/libs/webapi'
import countdown from 'src/components/countdown.vue'
// 初始化显示状态
function initState(mobile = true, serverPass = false, webPass = false, catchCode = false, msgCode = false, tip = false) {
  return {mobile, serverPass, webPass, catchCode, msgCode, tip}
}
// 初始化文本域值
function initFormData(mobile = '', serverPass = '', webPass = '', catchCode = '', msgCode = '') {
  return {mobile, serverPass, webPass, catchCode, msgCode}
}
const STEP = {CHECK: 1, /*检测手机号码*/ LOGIN: 2, /*登陆*/ RANDOMCODE: 3, /*特殊短信(江西电信)*/  BILL: 4   /*帐单*/}
const FiledName = {mobile: '手机号码', serverPass: '服务密码', webPass: '网站密码', msgCode: '短信验证', catchCode: '校验码'}

export default {
  data() {
    return {
      currentStep: STEP.CHECK,
      state: initState(),
      formData: initFormData(),
      toasts: [],
      catchCodeReload: true,
      msgCodeReload: false,
      catchCodeUrl: '',
      checkResult: null,
      indicator: {
        show: false,
        type: 'toast'
      },
      activeTimer: false, //短信成功 计时器
      ThirdVerificationCode: false, // 发送随机短信是否需要验证码
      nativeBriage: webApi.nativeBriage
    }
  },
  /**
   * 页面初始加载时检测手机号码
   * WebApi 网络连接失败后的统一处理
   */
  created: function () {
    //todo ajax请求失败 网络异常 统一处理?
    webApi.NetErrorCallBack = () => {
      this.showLoader(false)
      this.alert('无法连接服务器')
    }

    let _mobile = this.formData.mobile
    if (utils.isMobile(_mobile) && this.currentStep === STEP.CHECK) {
      this.handleSubmit()
    }
  },
  components: {
    countdown
  },
  watch: {
    nativeBriage: {
      handler: function (newVal, oldVal) {
        this.formData.mobile = newVal.authPNumber || ''
      },
      deep: true
    }
  },
  methods: {
    /** 手机号码 keyup事件处理 */
    mobileKeyup(){
      if (utils.isMobile(this.formData.mobile) && this.currentStep === STEP.CHECK) {
        this.handleSubmit()
      }
      if (this.currentStep === STEP.LOGIN) {
        this.state = initState()
        this.currentStep = STEP.CHECK
      }
    },
    /** 提交按钮事件处理 */
    handleSubmit() {
      switch (this.currentStep) {
        case STEP.LOGIN:
          this.handleLogin()
          break
        case STEP.RANDOMCODE:
          this.getRandomcode()
          break
        case STEP.BILL:
          this.handleBill()
          break
        case STEP.CHECK:
          this.handleCheckPhone()
          break
      }
    },
    /** toast提示 */
    alert(msg){
      this.toasts.push({text: msg, icon: 'error', center: true})
      // 3秒后自动消失
      setTimeout(() => this.toasts.splice(0, 1), 3000)
    },
    /** loader */
    showLoader (isShow = false, text = '加载中...') {
      this.indicator = {show: isShow, type: 'toast', text}
    },
    /** 表单重置 */
    resetForm(){
      this.formData = initFormData()
    },
    /** 表单验证*/
    checkForm(){
      let needChecks = Object.keys(this.state).filter((item) => this.state[item] === true)
      for (let i = 0; i < needChecks.length; i++) {
        let _key = needChecks[i]
        let _value = this.formData[needChecks[i]]
        // 手机号码校验
        if (_key === 'mobile' && utils.isMobile(_value) === false) {
          return '手机号码不正确'
        }
        // 通用验证规则 非空并且至少四位字符
        if (/\S{4,30}/g.test(_value) == false) {
          return `${FiledName[_key]} 不能为空`
        }
      }
      return null
    },
    /** 检查手机号码 */
    async handleCheckPhone() {
      //表单较验
      let checkResult = this.checkForm()
      if (checkResult !== null) {
        return this.alert(checkResult)
      }

      try {
        let phone = this.formData.mobile
        this.showLoader(true)
        let res = await webApi.mobile.checkPhoneNumber({phone})
        this.showLoader(false)
        // {"code":"800","msg":"非法请求，禁止访问","status":"904"}
        // {"code":"1","msg":"","status":"902"}

        if (!res || !res.body || res.code === '800' || res.status === '902') {
          return this.alert(res.msg);
        }

        switch (res.body.errorCode) {
          case '00000':
            this.state = initState(true, true, false, false, false, false)
            break
          case '10020':
            this.state = initState(true, false, true, false, false, false)
            break
          case '10021':
            this.state = initState(true, true, true, false, false, false)
            break
          case '10026':
            this.state = initState(true, true, false, false, true, false)
            break
          default:
            //todo 异常埋点 手机号检测出错处理 10001,10002,90000,90002
            return this.alert(res.msg);
        }
        //图片验证码
        if (res.body && res.body.result && res.body.result.captchaNeeded) {
          this.state.catchCode = true
          this.getCatchCode()
        }
        //保存result信息
        if (res.body && res.body.result) {
          this.checkResult = res.body.result
        }

        this.currentStep = STEP.LOGIN
      } catch (err) {

      }
    },
    /** 获取登陆图片验证码 */
    async getCatchCode() {
      try {
        this.catchCodeReload = true
        let res = (this.currentStep === STEP.BILL || this.currentStep === STEP.RANDOMCODE)
            ?
            await webApi.mobile.secondCaptcha()
            :
            await webApi.mobile.getCaptcha()
          ;

        this.catchCodeReload = false
        if (res.body.errorCode !== '00000') {
          //todo 异常埋点 图片验证码获取失败
          return this.alert('校验码请求失败,请点击重试')
        }
        this.catchCodeUrl = res.body.result
      } catch (err) {
      }
    },
    /** 获取登陆短信验证码 */
    async getRandomcode() {

      if (this.activeTimer === true) {
        return false
      }
      try {
        this.msgCodeReload = true
        let fData = this.formData
        let _param = {
          captchaCode: fData.catchCode
        }

        let res = this.currentStep === STEP.LOGIN
            ?
            await webApi.mobile.sendLoginRandomcode()
            :
            await webApi.mobile.sendRandomcode(_param)
          ;
        this.msgCodeReload = false

        if (res.body.errorCode !== '00000') {
          // todo 异常埋点 验证码请求失败
          this.alert('验证码请求失败,请点击重试')
          return
        }
        this.activeTimer = true

        if (this.currentStep == STEP.RANDOMCODE) {
          this.currentStep = STEP.BILL
          //抓取账单表单  激活倒计时
          this.billForm(true)
        }
      } catch (err) {
        this.msgCodeReload = false
      }
    },
    /** 登陆 */
    async handleLogin() {
      //表单较验
      let checkResult = this.checkForm()
      if (checkResult !== null) {
        return this.alert(checkResult)
      }
      try {
        let fdata = this.formData;
        let _param = {
          phone: fdata.mobile,
          password: fdata.serverPass,
          websitePassword: fdata.webPass,
          captchaCode: fdata.catchCode,
          randomCode: fdata.msgCode,
        }
        this.showLoader(true, '正在登陆,请耐心等待')
        let res = await webApi.mobile.login(_param)
        this.showLoader(false)

        if (res.body && res.body.errorCode && res.body.errorCode !== '00000') {
          this.alert(res.msg)
          this.getCatchCode()
          this.formData.catchCode = ''
          // todo 异常埋点 登陆失败
          return
        }
        this.handleBeforeBill()
      } catch (err) {
      }
    },
    /** 抓取帐单之前处理 */
    handleBeforeBill() {
      // 判断是否需要二次图片验证码 和 二次短信验证码
      let secondCaptchaNeeded = this.checkResult.secondCaptchaNeeded
      let randomSmsCodeNeeded = this.checkResult.randomSmsCodeNeeded
      //直接抓取帐单
      if (secondCaptchaNeeded === false && randomSmsCodeNeeded === false) {
        return this.handleBill()
      }
      //江西电信处理
      if (this.checkResult.carrier == "江西电信") {
        //江西电信
        this.state = initState(false, false, false, secondCaptchaNeeded)
        this.currentStep = STEP.RANDOMCODE
        //清空图片验证码
        this.emptyCatchCode()

        return
      }
      //抓取账单表单  不激活倒计时
      this.billForm(false)
    },
    /** 抓取账单表单 */
    billForm(_activeTimer) {
      // 判断是否需要二次图片验证码 和 二次短信验证码
      let secondCaptchaNeeded = this.checkResult.secondCaptchaNeeded
      let randomSmsCodeNeeded = this.checkResult.randomSmsCodeNeeded
      //非江西电信需输入短信验证码
      this.state = initState(false, false, false, secondCaptchaNeeded, randomSmsCodeNeeded)
      this.currentStep = STEP.BILL

      //清空图片验证码,并重新获取图片验证码
      if (secondCaptchaNeeded) {
        this.emptyCatchCode()
      }
      //清空短信验证码
      this.emptyMsgCode(_activeTimer)
    },
    emptyCatchCode() {
      this.formData.catchCode = ''
      this.getCatchCode()
    },
    emptyMsgCode(_activeTimer = false) {
      this.formData.msgCode = ''
      this.activeTimer = _activeTimer
    },

    /** 抓取帐单 */
    async handleBill() {

      let fData = this.formData
      let _param = {
        randomCode: fData.msgCode,
        captchaCode: fData.catchCode
      }
      let res = await webApi.mobile.bills(_param)

      if (res.body.errorCode !== '00000') {
        // todo 异常埋点 抓取数据失败
        return this.alert(res.msg)
      }
      // todo 抓取帐单成功后的响应状态
      window.location.href = '/interacts/success.html'
    }
  }

}
