import 'babel-polyfill'
import 'es6-promise'
import Vue from 'vue'
import FastClick from 'fastclick'
import App from './App'
import BaseComponent from 'src/components'
FastClick.attach(document.body)
Vue.use(BaseComponent)
/*eslint-disable*/
new Vue({
  el: 'body',
  components: {
    App
  }
})
/*eslint-enable*/

// 处理retina屏幕显示效果
var classNames = []
let pixelRatio = window.devicePixelRatio || 1
classNames.push('pixel-ratio-' + Math.floor(pixelRatio))
if (pixelRatio >= 2) {
  classNames.push('retina')
}

let html = document.getElementsByTagName('html')[0]

classNames.forEach((className) => html.classList.add(className))
