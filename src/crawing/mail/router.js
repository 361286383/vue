// 导入视图组件
import mailForm from './views/mailForm.vue'
import mailList from './views/mailList.vue'

export default function (router) {
  router.map({
    '/': {
      name: 'home',
      component: mailList
    },
    '/mail/:vender': {
      name: 'mailForm',
      component: mailForm
    }
  })
}
