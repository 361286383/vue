import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import routerMap from './router'
import BaseComponent from 'src/components'
// 注册通用组件
Vue.use(BaseComponent)
// 注册路由器插件
Vue.use(VueRouter)
// 创建路由实例
let router = new VueRouter({})
// 注册路由
routerMap(router)
// 启动app
router.start(App, 'app')
