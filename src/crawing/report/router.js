// 导入视图组件
import loginView from './views/loginView.vue'
import registerView from './views/registerView.vue'
import findnameView from './views/findnameView.vue'
import findpasswordView from './views/findpasswordView.vue'
import question from './views/question.vue'

export default function (router) {
  router.map({
    //login
    '/': {
      name: 'home',
      component: loginView
    },
    '/register': {
      name: 'register',
      component: registerView
    },
    '/findname': {
      name: 'findname',
      component: findnameView
    },
    '/findpassword': {
      name: 'findpassword',
      component: findpasswordView
    },
    '/question': {
      name: 'question',
      component: question
    }
  })
}
