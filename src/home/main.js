import Vue from 'vue'
import FastClick from 'fastclick'
import App from './App'
// button
import buttonArea from 'src/components/carbon/button/buttonArea'
import buttonRow from 'src/components/carbon/button/buttonRow'
import button from 'src/components/carbon/button/button'
import floatButton from 'src/components/carbon/button/floatButton'

// 注册为 vue 全局组件
const components = {
  'button-area': buttonArea,
  'button-row': buttonRow,
  button,
  'float-button': floatButton
}
Vue.use({
  install (Vue) {
    Object.keys(components).forEach((key) => Vue.component(key, components[key]))
  }
})

FastClick.attach(document.body)

export default new Vue({
  el: 'body',
  components: {
    App
  }
})
