// form
import textField from 'src/components/forms/textField.vue'
import formList from 'src/components/forms/formList.vue'
import formGroup from 'src/components/forms/formGroup.vue'
import checkbox from 'src/components/forms/checkbox.vue'
import itemForm from 'src/components/forms/itemForm.vue'
import button from 'src/components/button/button.vue'
import radio from 'src/components/forms/radio.vue'
// common
import toast from 'src/components/toast.vue'
import tip from 'src/components/tip.vue'
import icon from 'src/components/icon/icon.vue'
import indicator from 'src/components/indicator/indicator.vue'
import spinner from 'src/components/spinner.vue'
import selectField from 'src/components/forms/selectField.vue'
import popup from 'src/components/popup/popup.vue'
import picker from 'src/components/picker/picker.vue'
import countdown from 'src/components/countdown.vue'
import actionSheet from 'src/components/actionSheet/actionSheet.vue'

var components = {
  textField,
  formList,
  formGroup,
  itemForm,
  button,
  toast,
  tip,
  icon,
  indicator,
  spinner,
  selectField,
  popup,
  picker,
  checkbox,
  radio,
  countdown,
  actionSheet
}

export default {
  install (Vue) {
    Object.keys(components).forEach((key) => Vue.component(key, components[key]))
  }
}
