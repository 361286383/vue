
// 入口点配置
exports.entrys = {
  'index': 'src/home/main.js',
  'example': 'src/example/main.js',
  'crawing-bank': 'src/crawing/bank/main.js',
  'crawing-mobile': 'src/crawing/mobile/main.js',
  'crawing-mail': 'src/crawing/mail/main.js',
  'crawing-report': 'src/crawing/report/main.js',
  'crawing-zima': 'src/crawing/zima/main.js',
}
// html模板配置
exports.htmls = {
  'index': 'src/home/index.html',
  'example': 'src/example/index.html',
  'crawing-bank': 'src/crawing/bank/index.html',
  'crawing-mobile': 'src/crawing/mobile/index.html',
  'crawing-mail': 'src/crawing/mail/index.html',
  'crawing-report': 'src/crawing/report/index.html',
  'crawing-zima': 'src/crawing/zima/index.html',
}
