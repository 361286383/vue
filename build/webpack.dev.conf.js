var config = require('../config')
var webpack = require('webpack')
var merge = require('webpack-merge')
var utils = require('./utils')
var baseWebpackConfig = require('./webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
  baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

const devWebpackConfig = {
  module: {
    loaders: utils.styleLoaders({ sourceMap: config.dev.cssSourceMap })
  },
  // eval-source-map is faster for development
  devtool: '#eval-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env': config.dev.env
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
  ]
}

// https://github.com/ampedandwired/html-webpack-plugin
const htmls = require('./webpack-entry-html-conf.js').htmls

Object.keys(htmls).forEach(chunkName => {

  devWebpackConfig.plugins.push(
    new HtmlWebpackPlugin({
      filename: `${chunkName}.html`,
      template: htmls[chunkName],
      inject: true,
      // necessary to consistently work with multiple chunks via CommonsChunkPlugin
      chunks:['manifest','vendor',chunkName],
      chunksSortMode: 'dependency'
    })
  )
})

module.exports = merge(baseWebpackConfig, devWebpackConfig)
