# open-online-h5-vue


# 目录节构

```
.
├── build                       #构建脚本
│   ├── build.js
│   ├── dev-client.js             开发客户端热重载脚本
│   ├── dev-server.js             开发服务器脚本
│   ├── utils.js                  构建工具库
│   ├── webpack.base.conf.js      webpack基本配置
│   ├── webpack.dev.conf.js       开发环境下webpack配置覆盖 `webpack基本配置`
│   └── webpack.prod.conf.js      生产环境下webpack配置覆盖 `webpack基本配置`
│
├── config                      #环境配置目录(开发,测试,生产)
│   ├── dev.env.js
│   ├── index.js
│   ├── prod.env.js
│   └── test.env.js
│
├── src                         #源码目录
│   ├── components                  公共组件
│   │   ├── Hello.vue
│   │   └── RedBox.vue
│   └── crawing                     授信
│       ├── bank                        银行流水
│       │   ├── App.vue                   模块顶级组件
│       │   ├── assets                    模块私有资源
│       │   ├── components                模块私有组件
│       │   ├── index.html                模块模板页
│       │   └── main.js                   模块入口文件 用于webpack打包
│       ├── mail                        信用卡帐单
│       ├── mobile                      手机运营商帐单
│       ├── report                      征信报告
│       └── zima                        芝麻信用记录
│
├── static                      #通用静态资源
│
├── test                        #单元测试
│    └── unit
│        ├── index.js
│       ├── karma.conf.js
│       └── specs
│           └── Hello.spec.js
├── index.html
├── package.json
└── README.md

```

# git分支管理策略

### 分支说明

|分支 | push|delete |说明
|---|---
|master                  |否|否| 主分支 用于产品发布
|develop                 |否|否| 本分支主要是合并其子分支，平时不在这个分支开发
|develop-crawing-mobile  |是|否|手机运营商帐单分支
|develop-crawing-bank    |是|否|银行流水分支
|develop-crawing-mail    |是|否|信用卡帐单分支
|develop-crawing-report  |是|否|征信报告分支
|develop-crawing-zima    |是|否|芝麻信用分支

> 上面的这几个分支都是受保护分支，也就是说普通开发者是无权删除远程分支。

> push： 表示在当前分支下，普通开发者是否可以push

> delete： 表示普通开发者是否可以删除当前分支

**分支合并策略**
```
graph LR
develop子分支 --merge --> develop
develop--merge no-ff -->master
```
>一般我们主要在develop子分支下进行开发，比如 `develop-crawing-mobile`,在该子分支进行开发并提交，经测试没有问题之后，再合并到`develop`分支。每个`develop子分支`实际维护一个独立的功能模块，为了最大限度的减少冲突，开发者尽量只修改与该模块相关的内容即可。

**举例说明**

如果我们现在要修改`银行流水`模块




```bash
# step1 克隆工程
git clone http://gitlab.puhuitech.cn/puhui-fe/open-line-h5-vue.git

# step2 切换到 develop-crawing-bank分支
git checkout -b develop-crawing-bank

# step3 修改提交
git commit -m "修改并提交到develop-crawing-bank分支"
git push

# step4 切换到`develop`分支
git checkout -b develop

# step5 develop-crawing-bank合并到 `develop`
git merge develop-crawing-bank

```
> 注意

> 因为普通开发者(非owenr)不能在develop分支上push，所以需要到gitlab上提交 merge request,由 owner 完成审查合并

# 安装及配置
### 配置npm加速下载

```bash
# 让 npm 从淘宝镜像下载
npm config set disturl https://npm.taobao.org/dist
npm config set registry http://registry.npm.taobao.org

# 全局安装cnpm  用cnpm下载node-sass,phantomjs-prebuilt 非常快
npm install cnpm -g

# 如果本机已经启动了代理软件 如 lantern, 可以设置 npm 代理, 正常情况下通过上面两个配置就可以，实现不行可以试着配置代理
npm config set proxy http://127.0.0.1:8787
```

### 安装

```bash
git clone http://gitlab.puhuitech.cn/puhui-fe/open-line-h5-vue.git

npm install # 安装package.json列出的所有依赖库

# node-sass phantomjs-prebuilt 是项目依赖的必须安装，我们需要进行全局安装
# 这两个模块已从 package.js中移除，因为中国墙的原因 npm install会卡住，而用cnpm安装会非常快
cnpm install  -g node-sass phantomjs-prebuilt
```

### WebStrom 开发环境配置

1. 确定启用node环境 Language & FrameWorks -> Node And Npm -> Node Core Library enable 为 `enable`
2. Javascript Version 为 `EcmaScript 6`
3. Javascript -> Libray -> ECMAScript 6 勾选
4. 启用ESLint代码质量及代码风格检查 Javascript -> Code Qutity tool -> Eslint

# 开发



### 开发命令

|命令|说明
|---|---
| npm run dev| 运行该命令后，会监视所有 `src`目录下的改动，并自动增量编译到内存
| npm run build| 编译所有模块
| npm run test| 单元测试和代码覆盖率报告

### css编译

- less
- scss

编写css注意事项

1. 不要写内连样式特别是css3的属性 如  display:flex  background-size

```
//错误
<div style="display:flex; flex:1;"></div>

//正确
<div class="xx-xx-flex-1"></div>
<style>
.xx-xx-flex-1 {
  display:flex;
  flex:1;
}

</style>
```

# 测试



# 部署

我们在开发过程中是组件化方式开发,但是打包构建则是以资源形式,也就是说,所有编译的的js和css按照全部放到`static`目录,入口`html`全部放到站点根目录.
![image description](doc/dist.png)

1. 异常测试
2. 异常埋点


模块儿分配


模块 | 经办人
---|---
bank | 李培
report | 李培
mail| 谢强
mobile | 谢强
zima | 谢强



# 组件使用说明

[Vue Carbon 使用文档](https://myronliu347.github.io/vue-carbon/book/index.html)

